__author__ = 'Shelby'

import praw
from time import sleep
import random
from CREDENTIALS import *

LIMIT = 100
RESPONSES = ["Corgis are the 11th smartest dog breed ranked by professor Stanley Coren.",
             "Coming in at just a foot tall, the Pembroke Welsh Corgi is the smallest of the herding breeds.",
             "Queen Elizabeth II loves Corgis.",
             "Corgis evolved into two types, Cardigan and Pembroke.",
             "Corgis are really cute.",
             ]

responded = set()

r = praw.Reddit(user_agent="corgi-bot")
r.login(USERNAME, PASSWORD)
subreddit = r.get_subreddit("aww")

def meets_criteria(responded, comment):
    #add whatever criteria/logic you want here
    return (not str(comment.author) == USERNAME) and (not comment.id in responded)  and ("corgi" in comment.body.lower())

def generate_response(comment):
    #generate whatever response you want, you can make it specific to a comment by checking for various conditions
    return "Did you know, " + random.choice(RESPONSES)

while True:
    for comment in subreddit.get_comments(limit=LIMIT):
        if meets_criteria(responded, comment):
            print comment.body
            print comment.id
            print str(comment.author)
            while True: #continue to try responding to the comment until it works, unless something unknown occurs
                try:
                    comment.reply(generate_response(comment))
                    print "Breaking out after responding, and adding to the list"
                    responded.add(comment.id)
                    break
                except praw.errors.RateLimitExceeded:
                    print "Sleeping, rate limit :("
                    sleep(10*60) #sleep for 10 minutes, that's the timer limit
                except:
                    print "Some unknown error has occurred, bail out..."
                    break
            print "---------------------------------------\n\n"
    print "sleeping"
    sleep(60) #sleep for a minute for new comments to show up